# Use the official Nginx image as the base image
FROM nginx:alpine

# Copy the HTML, CSS, and any other static files to the appropriate directory in the container
COPY . /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;"]